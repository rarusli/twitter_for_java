package com.rudyrusli.twitter_for_java;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TwitterForJavaApplicationTests {

	@Test
	public void contextLoads() {
	}

}
