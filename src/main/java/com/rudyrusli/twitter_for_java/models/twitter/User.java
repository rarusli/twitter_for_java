package com.rudyrusli.twitter_for_java.models.twitter;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {

    @JsonProperty("id")
    public long id;

    @JsonProperty("id_str")
    public String idStr;

    @JsonProperty("name")
    public String name;

    @JsonProperty("screen_name")
    public String screenName;

    @JsonProperty("location")
    public String location;

    @JsonProperty("url")
    public String url;

    @JsonProperty("description")
    public String description;

    @JsonProperty("protected")
    public boolean protectedTweet;

    @JsonProperty("verified")
    public boolean verified;

    @JsonProperty("followers_count")
    public int followersCount;

    @JsonProperty("friends_count")
    public int friendsCount;

    @JsonProperty("listed_count")
    public int listedCount;

    @JsonProperty("favourites_count")
    public int favouritesCount;

    @JsonProperty("statuses_count")
    public int statusesCount;

    @JsonProperty("created_at")
    public String createdAt;

    @JsonProperty("profile_banner_url")
    public String profileBannerUrl;

    @JsonProperty("profile_image_url_https")
    public String profileImageUrlHttps;

    @JsonProperty("default_profile")
    public boolean defaultProfile;

    @JsonProperty("default_profile_image")
    public boolean defaultProfileImage;

    @JsonProperty("withheld_scope")
    public String withheldScope;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdStr() {
        return idStr;
    }

    public void setIdStr(String idStr) {
        this.idStr = idStr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isProtectedTweet() {
        return protectedTweet;
    }

    public void setProtectedTweet(boolean protectedTweet) {
        this.protectedTweet = protectedTweet;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(int followersCount) {
        this.followersCount = followersCount;
    }

    public int getFriendsCount() {
        return friendsCount;
    }

    public void setFriendsCount(int friendsCount) {
        this.friendsCount = friendsCount;
    }

    public int getListedCount() {
        return listedCount;
    }

    public void setListedCount(int listedCount) {
        this.listedCount = listedCount;
    }

    public int getFavouritesCount() {
        return favouritesCount;
    }

    public void setFavouritesCount(int favouritesCount) {
        this.favouritesCount = favouritesCount;
    }

    public int getStatusesCount() {
        return statusesCount;
    }

    public void setStatusesCount(int statusesCount) {
        this.statusesCount = statusesCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getProfileBannerUrl() {
        return profileBannerUrl;
    }

    public void setProfileBannerUrl(String profileBannerUrl) {
        this.profileBannerUrl = profileBannerUrl;
    }

    public String getProfileImageUrlHttps() {
        return profileImageUrlHttps;
    }

    public void setProfileImageUrlHttps(String profileImageUrlHttps) {
        this.profileImageUrlHttps = profileImageUrlHttps;
    }

    public boolean isDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(boolean defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public boolean isDefaultProfileImage() {
        return defaultProfileImage;
    }

    public void setDefaultProfileImage(boolean defaultProfileImage) {
        this.defaultProfileImage = defaultProfileImage;
    }

    public String getWithheldScope() {
        return withheldScope;
    }

    public void setWithheldScope(String withheldScope) {
        this.withheldScope = withheldScope;
    }
}
