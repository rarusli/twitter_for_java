package com.rudyrusli.twitter_for_java.models.twitter;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Tweets {

    @JsonProperty("statuses")
    private List<Tweet> tweets;

    public List<Tweet> getTweets() {
        return tweets;
    }

    public void setTweets(List<Tweet> tweets) {
        this.tweets = tweets;
    }

    @Override
    public String toString() {
        return "Tweets{" +
                "tweets=" + tweets +
                '}';
    }
}