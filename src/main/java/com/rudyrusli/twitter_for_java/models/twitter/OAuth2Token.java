package com.rudyrusli.twitter_for_java.models.twitter;

public class OAuth2Token {

    public String token_type;

    public String access_token;

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    @Override
    public String toString() {
        return "OAuth2Token{" +
                "token_type='" + token_type + '\'' +
                ", access_token='" + access_token + '\'' +
                '}';
    }
}
