package com.rudyrusli.twitter_for_java.error;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class MyResponseErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        boolean hasError = false;
        int rawStatusCode = response.getRawStatusCode();
        if (rawStatusCode != 200){
            hasError = true;
        }
        return hasError;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        int statusCode = response.getRawStatusCode();

        if ( statusCode != HttpStatus.OK.value() && statusCode != HttpStatus.NO_CONTENT.value() ) {
            String body = IOUtils.toString(response.getBody(), StandardCharsets.UTF_8);
            System.out.println("!!!!! ERROR :: " + response.getRawStatusCode() + " --> " + body);
        }
    }
}
