package com.rudyrusli.twitter_for_java.controller;

import com.rudyrusli.twitter_for_java.client.TwitterClient;
import com.rudyrusli.twitter_for_java.models.twitter.Tweet;
import com.rudyrusli.twitter_for_java.models.twitter.Tweets;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;

@Controller
@RequestMapping("/twitter")
public class ApiController {

    @Inject
    private TwitterClient twitterClient;

    @ApiOperation(value="Say Hello World", httpMethod="GET")
    @GetMapping(value = "/", produces = {"application/json"})
    @ResponseBody
    public String sayHello(
            @RequestParam(name="name", required=false, defaultValue="Stranger") String name) {
        return "Hi there " + name + " !";
    }

    @ApiOperation(value="Search Twitter", httpMethod="GET")
    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = {"application/json"})
    @ResponseBody
    public Tweets searchTwitter(
            @RequestParam(name="query", required=false, defaultValue="news") String query) {

        ResponseEntity<Tweets> entity = twitterClient.search(query);
        return entity.getBody();
    }

    @ApiOperation(value="Get user timeline", httpMethod="GET")
    @RequestMapping(value = "/user_timeline", method = RequestMethod.GET, produces = {"application/json"})
    @ResponseBody
    public Tweet[] getUserTimeline(
            @RequestParam(name="user_name", required=false, defaultValue="twitter_api") String query) {

        ResponseEntity<Tweet[]> entity = twitterClient.getUserTimeline(query);
        return entity.getBody();
    }

}
