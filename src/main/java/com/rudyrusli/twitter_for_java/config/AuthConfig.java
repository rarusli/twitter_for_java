package com.rudyrusli.twitter_for_java.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuthConfig {

    @Value(value = "${twitter.auth.bearerToken}")
    private String bearerToken;

    @Value(value = "${twitter.auth.consumerKey}")
    private String consumerKey;

    @Value(value = "${twitter.auth.consumerSecret}")
    private String consumerSecret;

    @Value(value = "${twitter.auth.oauth2.url.token}")
    private String oauth2TokenUrl;

    @Bean
    public AuthConfig securityConfig() {
        return new AuthConfig();
    }

    public String getBearerToken() {
        return bearerToken;
    }

    public void setBearerToken(String bearerToken) {
        this.bearerToken = bearerToken;
    }

    public String getConsumerKey() {
        return consumerKey;
    }

    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public void setConsumerSecret(String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }

    public String getOauth2TokenUrl() {
        return oauth2TokenUrl;
    }

    public void setOauth2TokenUrl(String oauth2TokenUrl) {
        this.oauth2TokenUrl = oauth2TokenUrl;
    }
}
