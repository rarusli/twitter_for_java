package com.rudyrusli.twitter_for_java.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value(value = "${swagger.contact-name}")
    private String contactName;

    @Value(value = "${swagger.contact-url}")
    private String contactUrl;

    @Value(value = "${swagger.contact-email}")
    private String contactEmail;

    @Value(value = "${swagger.title}")
    private String title;

    @Value(value = "${swagger.description}")
    private String description;

    @Value(value = "${swagger.patterns}")
    private String patterns;

    @Bean
    public Docket api() {
        ApiInfo apiInfo = new ApiInfoBuilder()
                .contact(new Contact(contactName, contactUrl, contactEmail))
                .title(title)
                .description(description)
                .build();

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(patterns != null ? PathSelectors.regex(patterns) : PathSelectors.any())
                .build();
    }
}