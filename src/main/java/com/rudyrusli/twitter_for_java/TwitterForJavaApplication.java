package com.rudyrusli.twitter_for_java;

import com.rudyrusli.twitter_for_java.client.TwitterClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.inject.Inject;
import java.util.Scanner;

@SpringBootApplication
public class TwitterForJavaApplication implements CommandLineRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(TwitterForJavaApplication.class);

	@Inject
	private TwitterClient twitterClient;

	public static void main(String[] args) {
		SpringApplication.run(TwitterForJavaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		try {
			Scanner scanner = new Scanner(System.in);

			while (true) {
				System.out.println("What do you want to search on Twitter?");
				String name = null;
				if (scanner.hasNext()) {
					name = scanner.nextLine();
				}
				twitterClient.search(name);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.error("Error happens", ex);
		}
	}
}
