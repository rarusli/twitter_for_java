package com.rudyrusli.twitter_for_java.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rudyrusli.twitter_for_java.config.AuthConfig;
import com.rudyrusli.twitter_for_java.error.MyResponseErrorHandler;
import com.rudyrusli.twitter_for_java.models.HttpResponse;
import com.rudyrusli.twitter_for_java.models.twitter.OAuth2Token;
import com.rudyrusli.twitter_for_java.models.twitter.Tweet;
import com.rudyrusli.twitter_for_java.models.twitter.Tweets;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;

@Service
public class TwitterClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(TwitterClient.class);

    @Inject
    private AuthConfig securityConfig;

    private RestTemplate restTemplate = new RestTemplate();

    private ObjectMapper mapper = new ObjectMapper();

    @PostConstruct
    private void setupClient() {
        restTemplate.setErrorHandler(new MyResponseErrorHandler());
    }

    /**
     *
     * @param searchQuery
     * @throws Exception
     */
    public ResponseEntity search(String searchQuery) {
        LOGGER.info("Searching for '" + searchQuery + "'");

        try {
            String url = "https://api.twitter.com/1.1/search/tweets.json?q=" + searchQuery + "&result_type=popular";

            HttpHeaders headers = authorizeUsingOauth2();

            ResponseEntity<Tweets> responseEntity = restTemplate.exchange(
                    url, HttpMethod.GET,
                    new HttpEntity<>("", headers),
                    Tweets.class);

            printResponse(responseEntity);

            return responseEntity;
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Error " + ex.getMessage());
        }
    }

    public ResponseEntity getUserTimeline(String screenName) {
        LOGGER.info("Get user timeline for '" + screenName + "'");

        try {
            String url = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=" + screenName;// + "&count=2";

            HttpHeaders headers = authorizeUsingOauth2();

            ResponseEntity<Tweet[]> responseEntity = restTemplate.exchange(
                    url, HttpMethod.GET,
                    new HttpEntity<>("", headers),
                    Tweet[].class);

            printResponse(responseEntity);

            return responseEntity;
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Error " + ex.getMessage());
        }
    }

    /**
     *
     * @return
     */
    private HttpHeaders authorizeUsingOauth2() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + securityConfig.getBearerToken());
        return headers;
    }

    /**
     *
     * @return
     */
    private String getOauth2Token() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        String consumerKeyAndSecret = securityConfig.getConsumerKey() + ":" + securityConfig.getConsumerSecret();
        byte[] encodedBytes = Base64.encodeBase64(consumerKeyAndSecret.getBytes());

        headers.set("Authorization", "Basic " + new String(encodedBytes));
        headers.set("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                securityConfig.getOauth2TokenUrl() , HttpMethod.POST,
                new HttpEntity<>("grant_type=client_credentials", headers),
                String.class);

        String responseBody = responseEntity.getBody();
        OAuth2Token token = mapper.readValue(responseEntity.getBody(), OAuth2Token.class);

        LOGGER.info("OAuth2 Token is " + token);

        return responseBody;
    }

    /**
     *
     * @param responseEntity
     * @throws Exception
     */
    private void printResponse(ResponseEntity responseEntity) throws Exception {
        HttpResponse httpResponse = new HttpResponse(responseEntity.getStatusCode(),
                String.valueOf(responseEntity.getBody()));
        LOGGER.info("Response is " + httpResponse);
    }
}
